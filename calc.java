public class calc {
	
	public static void main(String[] args)
	{
		System.out.println(add(9.0,3.0));
		System.out.println(minus(9.0,3.0));
		System.out.println(mult(9.0,3.0));
		System.out.println(div(9.0,3.0));
	}

	public static double add(double a, double b)
	{
		return a+b;
	}
	
	public static double minus(double a, double b)
	{
		return a-b;
	}
	
	public static double mult(double a, double b)
	{
		return a*b;
	}
	
	public static double div(double a, double b)
	{
		return a/b;
	}
}
