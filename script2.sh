#!/bin/bash

cp /temp/shared/java.tar.gz /opt
cp /temp/shared/maven.tar.gz /opt

cd /opt

#unzip files
sudo tar zxvf java.tar.gz
sudo tar zxvf maven.tar.gz

#install java and maven
sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_45/bin/java 100
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_45/bin/javac 99
sudo update-alternatives --install /usr/bin/maven maven /opt/apache-maven-3.3.9/bin/mvn 98

#install javac
#sudo apt-get update
#sudo apt-get install -y default-jdk

#install git
sudo apt-get install -y git

#install jenkins
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins

#output to console installations
java -version 
javac -version 
maven -version
git --version
