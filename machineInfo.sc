#!/bin/bash

echo "you are about to log the users and the ip adress of the current machine."
echo "do you wish to over-write the previous logs? {y} {n}"
i=1
read choice
while [ $i -eq 1 ]
do
	if [ $choice == "y" ]
	then
		echo `who` > logs.txt
		echo `hostname -I` >> logs.txt
		((i--))
	else
		if [ $choice == "n" ]
		then
			echo `who` >> logs.txt
			echo `hostname -I` >> logs.txt
			((i--))
		else
			echo "do you wish to over-write the previous logs? {y} {n}"
			read choice
		fi
	fi
done
