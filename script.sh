#!/bin/bash

cp /temp/shared/java.tar.gz /opt
cd /opt

#unzip file
sudo tar zxvf java.tar.gz

#install java
sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_45/bin/java 100
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_45/bin/javac 99

#install git
sudo apt-get install -y git

#output to console installations
java -version 
javac -version 
git --version
